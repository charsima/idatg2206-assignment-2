
function img_data_classified = custom_sam_classify (img_data, referenceSpectrumY, lowerBoundary, upperBoundary)
    dimensions = size(img_data);
    img_data_classified = zeros(dimensions(1), dimensions(2));
    
    for x = 1:1:dimensions(1)
        for y = 1:1:dimensions(2)
            targetSpectrumY = get_spectrum(img_data, x, y);
            
            angle = custom_sam(referenceSpectrumY, targetSpectrumY);
            
            if angle <= upperBoundary && angle >= lowerBoundary
               img_data_classified(x, y) = 1; 
            end
        end
    end
end