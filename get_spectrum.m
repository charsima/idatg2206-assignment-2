
function spectrumY = get_spectrum (img_data, pixelX, pixelY)
    dimensions = size(img_data);
    spectrumY = zeros(dimensions(3), 1);
    
    for i = 1:1:dimensions(3)
        spectrumY(i) = img_data(pixelX, pixelY, i);
    end
end