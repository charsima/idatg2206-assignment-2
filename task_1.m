img = hypercube("oblig2.img", "oblig2.hdr");
spectrum = zeros(img.Metadata.Bands, 3);

% Area 1
spectrum1Y = get_average_spectrum(img.DataCube(1:250, :, :));

figure("Name", "Area 1");
plot(img.Wavelength, spectrum1Y);
title("Spectrum");
xlabel("Wavelength");
ylabel("Reflectance");

% Area 2
spectrum2Y = get_average_spectrum(img.DataCube(250:500, 1:250, :));

figure("Name", "Area 2");
plot(img.Wavelength, spectrum2Y);
title("Spectrum");
xlabel("Wavelength");
ylabel("Reflectance");

% Area 3
spectrum3Y = get_average_spectrum(img.DataCube(250:500, 250:500, :));

figure("Name", "Area 3");
plot(img.Wavelength, spectrum3Y);
title("Spectrum");
xlabel("Wavelength");
ylabel("Reflectance");
