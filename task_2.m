% Run task_1.m before running this (variables are requires from task 1)

classify1 = custom_sam_classify(img.DataCube, spectrum1Y, 0, 0.05);
classify2 = custom_sam_classify(img.DataCube, spectrum2Y, 0.08, 0.1);
classify3 = custom_sam_classify(img.DataCube, spectrum3Y, 0, 0.1);

figure;
imshow(classify1);
figure;
imshow(classify2);
figure;
imshow(classify3);