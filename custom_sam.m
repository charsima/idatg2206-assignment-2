
function angle = custom_sam(referenceSpectrumY, targetPixelSpectrumY)
    dimensions = size(referenceSpectrumY);
    bands = dimensions(1);
    
    topFraction = 0;
    bottomPart1 = 0;
    bottomPart2 = 0;
    
    for i = 1:1:bands
        topFraction = topFraction + targetPixelSpectrumY(i) * referenceSpectrumY(i);
        
        bottomPart1 = bottomPart1 + targetPixelSpectrumY(i)^2;
        bottomPart2 = bottomPart2 + referenceSpectrumY(i)^2;
    end
    
    bottomFraction = sqrt(bottomPart1) * sqrt(bottomPart2);
    
    angle = acos(topFraction / bottomFraction);
end