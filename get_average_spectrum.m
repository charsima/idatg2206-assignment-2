
function spectrumY = get_average_spectrum (area_data)
    dimensions = size(area_data);
    spectrumY = zeros(dimensions(3), 1);

    for i = 1:1:dimensions(3)
        for x = 1:1:dimensions(1)
            for y = 1:1:dimensions(2)
                spectrumY(i) = spectrumY(i) + area_data(x, y, i);
            end
        end
        
         spectrumY(i) =  spectrumY(i) / (dimensions(1) * dimensions(2));
    end
end